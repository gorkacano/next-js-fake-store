FROM node:18

RUN npm -g install npm@latest @angular/cli
USER node
WORKDIR /app
# RUN ng new pilotraf-frontend
# WORKDIR /usr/nextjs-fake-store
# RUN ng analytics off
# EXPOSE 4200
# COPY . .

# EXPOSE 5000

# CMD [ "npm", "run", "dev" ]
# ENTRYPOINT [ "/bin/bash" ]

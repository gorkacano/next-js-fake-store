import { type GetStaticPropsContext } from 'next'
import Image from 'next/image'
import { type Product } from '../Product'

// export default async function ProductPage ({ params }: GetStaticPropsContext): Promise<JSX.Element> {
const ProductPage = async ({ params }: GetStaticPropsContext): Promise<JSX.Element> => {
  if (params?.id === undefined || typeof (params.id) !== 'string') return (<div>Params.id must exist</div>)
  const product = await fetchProductById(params.id)
  return (
    <div
      id='products-container'
    >
      <h1>Product {params?.id}</h1>
      <section className=''>
        <article key={product.id}>
          <h2>{product.title}</h2>
          <Image width='100' height='100' src={product.image} alt='' />
          <p>{product.description}</p>
        </article>
      </section>
    </div>
  )
}

const fetchProductById = async (id: string): Promise<Product> => {
  return await fetch(`https://fakestoreapi.com/products/${id}`)
    .then(async (res) => await res.json())
    .catch((err) => {
      console.log(err)
    }
    )
}

export default ProductPage

import Image from 'next/image'
import Link from 'next/link'
import { type Product } from './Product'

export default async function ProductsPage (): Promise<JSX.Element> {
  const products = await fetchProducts()
  return (
    <div
      id='products-container'
    >
      <h1>Products</h1>
      <section className='grid grid-cols-4 gap-4'>
        {products.slice(0, 8).map((product) => (
          <article key={product.id}>
            <h2>
              <Link href={'products/' + product.id.toString()}>
                {product.title}
              </Link>
            </h2>
            <Image width='100' height='100' src={product.image} alt='' />
            <p>{product.description}</p>
          </article>
        ))}
      </section>
    </div>
  )
}

const fetchProducts = async (): Promise<Product[]> => {
  return await fetch('https://fakestoreapi.com/products')
    .then(async (res) => await res.json())
}
